import java.util.Scanner;

public class Fahrkartenautomat06 {
	 public static void main(String[] args)
	    {
		 
		 boolean kaufen=true;
		 String weiterKaufen;
		 
		 Scanner tastatur = new Scanner(System.in);
		 while(kaufen==true) {
	       double zuZahlenderBetrag=0.0; 
	       double eingezahlterGesamtbetrag=0.0;
	      
	       zuZahlenderBetrag= fahrkartenbestellungErfassen();
	       
	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag=fahrkartenBezahlen(zuZahlenderBetrag);
	      
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();

	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(eingezahlterGesamtbetrag - zuZahlenderBetrag);
	       // Weiteren Kaufvorgang durchf�hren?
		   System.out.println("\nM�chten Sie weitere Fahrkarten kaufen? \nZustimmung mit \"Ja\" als Eingabe");
		   weiterKaufen= tastatur.next();
		   if(weiterKaufen.equals("Ja")) {
			   kaufen=true;
		   }
		   else {
			   System.out.print("Nicht \"Ja\" eingegeben, weitere Kaufvorg�nge werden nicht durchgef�hrt. Wir w�nschen Ihnen eine gute Fahrt.");
			   kaufen=false;
		   }
		   
		 }
	    }
	     
	    public static double  fahrkartenbestellungErfassen() {
	    	
	    double zuZahlenderBetrag=0;
	    double anzahlDerTickets=0;
	    int ticketWahl=0;
	    boolean eingabeErfassen=true;
	    Scanner tastatur = new Scanner(System.in);
	    
	    while(eingabeErfassen==true) {
	    	
	     System.out.print("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
	     System.out.print("\nEinzelfahrscheine (1) ");
	     System.out.print("\nTageskarten (2)");
	     System.out.print("\nKleingruppen-Tageskarten (3)");
	     
	     ticketWahl=tastatur.nextInt();
	     
	     if(ticketWahl==1) {
	    	 zuZahlenderBetrag=2.90;
	    	 eingabeErfassen=false;
	     }
	     else if(ticketWahl==2){
	    	 zuZahlenderBetrag=8.60;
	    	 eingabeErfassen=false;
	     }
	     else if(ticketWahl==3) {
	    	 zuZahlenderBetrag=23.50;
	    	 eingabeErfassen=false;
	     }
	     else {
	    	 System.out.print("\nFalsche Eingabe. G�ltige Eingaben sind 1-3. Bitte nochmal eingeben");
	    	 eingabeErfassen=true;
	     }
	     
	    }
	    
	     
	     System.out.print("Anzahl der Tickets: ");
	     anzahlDerTickets = tastatur.nextInt();
	     
	     if(anzahlDerTickets <0 || anzahlDerTickets > 10) {
	    	 System.out.println("\nUng�ltige Ticket Anzahl, bitte zwischen 1 und 10 nochmal waehlen");
	    	 return fahrkartenbestellungErfassen();
	    	 
	     }
	     
	    return zuZahlenderBetrag*anzahlDerTickets;
	     }
	    
	    public static double fahrkartenBezahlen(double zuZahlen) {
	    	Scanner tastatur = new Scanner(System.in);
	    	
	    	double eingezahlterGesamtbetrag = 0.0;
	        while(eingezahlterGesamtbetrag < zuZahlen)
	         {
	      	   System.out.print("Noch zu zahlen: ");
	      	   System.out.printf("%.2f�\n",(zuZahlen - eingezahlterGesamtbetrag));
	      	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	      	   double eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	         }
	         
	         return eingezahlterGesamtbetrag;
	    
	    }
	    
	    public static void fahrkartenAusgeben () {
	    	System.out.println("\nFahrschein wird ausgegeben");
	        for (int i = 0; i < 8; i++)
	        {
	           System.out.print("=");
	           warte(250);
	        }
	        System.out.println("\n\n");
	    }
	    
	    public static void warte(int millisekunde) {
	        try {
	 			Thread.sleep(millisekunde);
	 		} catch (InterruptedException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		}	    }
	    
	    public static void rueckgeldAusgeben (double r�ckgabebetrag) {
	        if(r�ckgabebetrag > 0.0)
	        {
	     	   System.out.print("Der R�ckgabebetrag in H�he von ");
	     	   System.out.printf("%.2f�",r�ckgabebetrag);
	     	   System.out.println(" ");
	     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	            while(r�ckgabebetrag >= 1.99) // 2 EURO-M�nzen
	            {
	            	 muenzeAusgeben(2,"EURO");
	 	          r�ckgabebetrag -= 2.0;
	 	        
	            }
	            while(r�ckgabebetrag >= 0.99) // 1 EURO-M�nzen
	            {
	            	 muenzeAusgeben(1,"EURO");
	 	          r�ckgabebetrag -= 1.0;
	            }
	            while(r�ckgabebetrag >= 0.49) // 50 CENT-M�nzen
	            {
	            	 muenzeAusgeben(50,"CENT");
	 	          r�ckgabebetrag -= 0.5;
	            }
	            while(r�ckgabebetrag >= 0.19) // 20 CENT-M�nzen
	            {
	            	 muenzeAusgeben(20,"CENT");
	  	          r�ckgabebetrag -= 0.2;
	  	          
	            }
	            while(r�ckgabebetrag >= 0.09) // 10 CENT-M�nzen
	            {
	            	 muenzeAusgeben(10,"CENT");
	 	          r�ckgabebetrag -= 0.1;
	            }
	            while(r�ckgabebetrag >= 0.049)// 5 CENT-M�nzen
	            {
	             muenzeAusgeben(5,"CENT");
	  	          r�ckgabebetrag -= 0.05;
	            }
	        }

	        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                           "vor Fahrtantritt entwerten zu lassen!\n"+
	                           "Wir w�nschen Ihnen eine gute Fahrt.");
	    }
	    
	    public static void muenzeAusgeben(double betrag, String einheit) {
	    	
	    	 System.out.printf("\n%.0f %s",betrag, einheit);
	    }
}
